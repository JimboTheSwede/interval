# -*- coding: utf-8 -*-
"""
Created on Mon Mar 23 23:50:27 2015

@author: plu
"""

import time
import io
import re

import numpy as np
import pandas as pd

def unzip(xs):
    N = len(xs[0])
    ys = []
    [ys.append([]) for i in range(N)]

    for x in xs:
        for i, y in enumerate(x):
            ys[i].append(y)
    return ys
    
class LogException(Exception):
    pass

class LogIncomplete(LogException):
    pass

class SessionIncomplete(LogException):
    pass

class ResponceIncomplete(LogException):
    pass

class LogParser(object):
    
    def __init__(self, filename=None, dataFile=None, debug=False): 
        self.filename = filename
        self.dataFile = dataFile
        self.debug = debug 
        
        if filename:
            self.parseFile(filename)

    def _timeStamp2float(self, s):
        s,a,f = s.rpartition('.')
        tr = time.strptime(s, '%Y.%m.%d %H:%M:%S')
        return time.mktime(tr) + float(a+f)

    def _parseLine(self, l):
        c = l.split(',',2)
        t = self._timeStamp2float(c[0])
        key = c[1].strip()
        args = c[2].strip()

        reInt = re.compile("^\d+$")
        #TODO: implement scientific format
        reFloat = re.compile("^-?\d+\.\d+$")        
        if args[0] == '[':
            args = args.strip('[]')
            al = args.split(',')
            args = []
            
            for a in al:
                aa = a.strip()
                if not reFloat.match(aa) is None:
                    aa = float(aa)
                elif not reInt.match(aa) is None:
                    aa = int(aa)
                args.append(aa)
        else:
            args.strip()
            if not reFloat.match(args) is None:
                args = float(args)
            elif not reInt.match(args) is None:
                args = int(args)
        return (t, key, args)

    def getData(self, sheet, index):
        return pd.read_excel(self.dataFile, sheet, index_col=index)

    def parseFile(self, filename):
        with io.open(filename, 'r') as file:
            logTxt = file.read()

        self.log = [self._parseLine(l) for l in logTxt.splitlines()]
        return self.log

    def search(self, pattern, pos=0, end=None):
        if end is None:
            slice = self.log[pos:]
        else:
            slice = self.log[pos: end]
        rpat = re.compile(pattern)
        for i, l in enumerate(slice):
            if not rpat.match(l[1]) is None: 
                return l, pos + i + 1
        return None, pos

class RateLogParser(LogParser):
    def __init__(self, filename=None, dataFile=None):
        super(RateLogParser, self).__init__(filename, dataFile)

        self.parse()

    def parse(self):
        pos = 0
        l, fEnd = self.search('system.finished', pos)

        if l is None:
            raise LogIncomplete('Log is incomplete: %s' % self.filename)
        
        l, pos = self.search('subj.handler.rate', pos)

        l, pos = self.search('system.subject', pos, fEnd)
        subj = l[2]
        l, pos = self.search('system.session.begin', pos, fEnd)
        sess = l[2] + 1
        l, sEnd = self.search('system.session.end', pos)
        if l is None or l[2] != sess - 1:
            raise SessionIncomplete('Session %d is incomplete in: %s' % (sess, self.filename))

        l, pos = self.search('system.session.stimuli', pos, sEnd)
        stims = l[2]
        l, pos = self.search('system.session.excerpts', pos, sEnd)
        exces = l[2]

        items = {}
        while not l is None:
            l, pos = self.search('subj.item.up', pos, sEnd)
            if not l is None:
                items[l[2][0]] = l[2][1:]
                
        if len(items) != len(stims):
            raise ResponceIncomplete('Responce is incomplete in log: %s' % self.filename)

        self.makeDataFrame(subj, sess, items, stims, exces)
        
    def makeDataFrame(self, subj, sess, items, stims, exces):
        stimuli = self.getData('Stimuli', None)
        #excerpts = self.getData('Excerpts', None)
        #e = excerpts.Excerpt[exces]
        
        #if not (exce == e).all():
        #    raise Exception('Stimuli vs excerpt data is not consistant')

        idx = items.keys()
        N = len(idx)
        
        ss = pd.Series([stimuli.Stimuli[stims[i-1]] for i in idx], name='Stimuli')
        es = pd.Series([stimuli.Excerpt[stims[i-1]] for i in idx], name='Excerpt')
        df = pd.DataFrame(items.values(), columns=['Pleasentness', 'Eventfulness'], index=None)
        subj = pd.Series(np.full([N],subj, np.int64),name='Subject')
        sess = pd.Series(np.full([N],sess, np.int64), name='Session')

        self.df = pd.concat([df, ss, es, subj, sess], axis=1)

class ScaleLogParser(LogParser):

    sliderTags = [u'Trivsam', u'Kaotisk', u'Spännande', u'Händelselös',
                           u'Lugnt och stilla', u'Störande', u'Händelserik', u'Enformig']
    
    def __init__(self, filename=None, scaleNames=None, dataFile=None):
        super(ScaleLogParser, self).__init__(filename, dataFile)
        if scaleNames is None:
            self.scaleNames = self.sliderTags
        else:
            self.scaleNames = scaleNames
        if not filename is None:
            self.parse()

    def getScaleNames(self):
        data = self.getData('ScaleNames', 'Index')
        return data.Name.values

    def parse(self):
        pos = 0

        l, end = self.search('system.session.end', pos)
        if l is None:
            raise SessionIncomplete('Session is incomplete in: %s' % self.filename)
        l, pos = self.search('system.subject', pos)
        subj = l[2]
        l, pos = self.search('system.session.begin', pos)
        sess = l[2] + 1
    
        l, pos2 = self.search('system.subject', pos)
        
        l, pos = self.search('system.session.stimuli', pos, end)
        stims = l[2]
        l, pos = self.search('system.session.excerpts', pos, end)
        exces = l[2]
        vals = []
        while not l is None:
            l, pos = self.search('subj.stim', pos, end)
            if l is None:
                break
            stim = int(l[2])
            l, pos = self.search('subj.excerpt', pos, end)
            exce = int(l[2])
            l, pos = self.search('subj.slider.indices', pos, end)
            idxs = np.array(l[2])
            l, pos = self.search('subj.slider.values', pos, end)
            v = np.array(l[2])[idxs.argsort()]
            v = np.append(v, stim)
            v = np.append(v, exce)
            vals.append(v)
        vals = np.asfarray(vals)
        self.vals = vals
        
        #self.df = pd.DataFrame(vals, columns=self.scaleNames, index=None)
        self.makeDataFrame(subj, sess, stims, exces, vals)


    def makeDataFrame(self, subj, sess, stim, exce, vals):
        stimuli = self.getData('Stimuli', None)
        stix = np.asarray(vals[:,8], np.int64)
        exix = np.asarray(vals[:,9], np.int64)
        stim = stimuli.Stimuli[stix]
        N = len(stim)
        stim.index = range(N)
        exce = stimuli.Excerpt[stix]
        excerpts = self.getData('Excerpts', None)
        e = excerpts.Excerpt[exix]

        if not (exce == e).all():
            #print e
            #print exce
            #print type(exce)
            raise Exception('Stimuli vs excerpt data is not consistant')
        exce.index = range(N)
        columns = self.getScaleNames()
        columns = np.append(columns, 'Stimuli')
        columns = np.append(columns, 'Excerpt')
        df = pd.DataFrame(vals[:,:8], columns=columns[:8], index=None)
        subj = pd.Series(np.full([N],subj, np.int64),name='Subject')
        sess = pd.Series(np.full([N],sess, np.int64), name='Session')
        self.df = pd.concat([df,stim, exce, subj, sess], axis=1)
        

#TODO: Stimuli and excerpt columns seams to be wrong!
#TODO: check if log is complete!

if __name__ == "__main__":
    import os

    DATAFILE = os.path.expanduser('~/Projects/soundcities/sc_ex005/data/sc_ex005.xlsx')
    
    def writeToExcel(df, filename, sheet='Sheet1'):
        df.to_excel(filename, sheet_name=sheet)

    def getData(dataFile, sheet, index):
        return pd.read_excel(dataFile, sheet, index_col=index)

    def getTestOrder(data, subj):
        return data.Test1[subj]

    
    def getHandler(filename):
        with open(filename,'r') as fh:
            ls = fh.readlines(10)
            for l in ls[:10]:
                k = l.split(',')[1]
                if 'subj.handler' in k:
                    return k.split('.')[2]

    def filterFiles(fileList):
        res = []
        for f in fileList:
            if (f.startswith('sc_ex005') and
                    f.endswith('.log') and
                    not 'subj99' in f):
                res.append(f)
        return res
             
    logDir = os.path.expanduser('~/Projects/soundcities/sc_ex005/log')

    #logFile=os.path.join(logDir,'sc_ex005_subj1011_01.log')
    #log = ScaleLogParser(logFile)

    ORDER = getData(DATAFILE, 'Subjects', 'Id')

    STIMULI = getData(DATAFILE, 'Stimuli', None)

    fl = os.listdir(logDir)
    fl.sort()
    fl.reverse()
    fl = filterFiles(fl)

    #fl = ['sc_ex005_subj1034_01.log', 'sc_ex005_subj1033_01.log']
    ssDone = set()
    dfScales = None
    dfRate = None

    for f in fl:
        #try:
            print('Processing log: %s' % f)
            fs = f.split('_')
            subjIn = int(fs[2][4:])
            if subjIn in ssDone:
                continue
            vers = int(fs[3].split('.')[0])

            subj = subjIn / 10
            sess = subjIn % 10

            order = getTestOrder(ORDER, subj)
            type_ =  getHandler(os.path.join(logDir, f))

            if (sess < 3 and order == 0) or (sess >= 3 and order == 1):
                test = 'RATE'

                try:
                    log = RateLogParser(os.path.join(logDir, f), dataFile=DATAFILE)
                except LogException as e:
                    print(e)
                    continue
                except Exception as e:
                    raise
                    continue

                if dfRate is None:
                    dfRate = log.df
                else:
                    dfRate = pd.concat([dfRate, log.df], axis=0)
                    #print df

            else:
                test = 'SCALE'

                try:
                    log = ScaleLogParser(os.path.join(logDir, f), dataFile=DATAFILE)
                except LogException as e:
                    print(e)
                    continue
                except Exception as e:
                    raise e
                    continue


                if dfScales is None:
                    dfScales = log.df
                else:
                    dfScales = pd.concat([dfScales, log.df], axis=0)
                    #print df
            ssDone.add(subjIn)
        #except Exception as e:
            #print e

    writeToExcel(dfScales, '/tmp/testS.xlsx', 'Scale')
    writeToExcel(dfRate, '/tmp/testR.xlsx', 'Rate')
    
    '''
    def cntStim(d):
        r = {}
        for e in d:
            stim = e[2]
            if stim in r:
                r[stim] += 1
            else:
                r[stim] = 1
        return r
        
    #m = '2015.03.23 23:48:45.731, subj.item.down, -1'
    #d = LogParser._parseLine(m)

    lp=LogParser(log)

    #print lp.getSessions()
    
    data = lp.getFinalPositionData()

    print cntStim(data)
    print lp.getSessions()
    '''

