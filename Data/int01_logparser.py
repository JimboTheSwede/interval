import os
import sys
import ast
import traceback
import time

import numpy as np
import pandas as pd

from logparser import LogParser

LOGDIR = os.path.normpath('D:\Repos\Interval\Loggar\Log_2017_03_28')
DATAFILE = 'int01_stimuli_v1.xlsx'

PREFIX = 'int01'

def filterFiles(fileList, prefix):
    res = []
    for f in fileList:
        if (f.startswith(prefix) and
                f.endswith('.log') and
                not 'subj99' in f):
            res.append(f)
    return res

def scanDir(dir, prefix):
    fl = os.listdir(dir)
    fl.sort()
    fl.reverse()
    fl = filterFiles(fl, prefix)
    return fl

def parseSubjectId(filename):
    fs = filename.split('_')
    return int(fs[1][4:])

def float2excelTime(t):
    return time.strftime('%Y-%m-%d %H:%M',time.localtime(t))

class IncompleteLogError(Exception):
    pass

class IntervalLogParser(LogParser):

    def getEventData(self, tag, pos, end):
        l, pos = self.search(tag, pos, end)
        val = None
        if l is None:
            print('Warning: %s is missing!' % tag)
        else:
            val = l[2]
        return val, pos
        
    def parse(self):
        self.debug = False
        if self.debug:
            print 'IntervalLogParser.parse()'
            
        data = self.getData('Stimuli', None)
        pos = 0
        l, pos = self.search('system.begin', pos)
        l, pausePos = self.search('subject.pause', pos)
        l, fEnd = self.search('subject.end', pos)
        if l is None:
            print('Warning: Log is incomplete: %s' % self.filename)
            fEnd = len(self.log)
        l, pos = self.search('system.version', pos, fEnd)
        
        version, pos = self.getEventData('system.operator', pos, fEnd)
        subject, pos = self.getEventData('system.subject', pos, fEnd)
        if subject is None:
            subject = parseSubjectId(self.filename)
        
        l, pos = self.search('subject.begin', pos, fEnd)
        if l is None:
            raise IncompleteLogError('No subject.begin event found')
        date = float2excelTime(l[0])
        print date
        sessionType = l[2]
        l, pos = self.search('system.stimuliset', pos, fEnd)
        stimuli = l[2]
        self.df = None
        sEnd = pausePos
        sn = 0
        while pos < fEnd:
            
            sn += 1
            vs = [] 
            while not l is None and pos < sEnd:
                
                l, pos = self.search('subject.next', pos, sEnd)
                if l is None:
                    break
                d = ast.literal_eval(l[2])
                if sessionType == 'A':
                    vs.append(d['high'])
                else:
                    vs.append((d['low'], d['high']))
            p = pd.Series(stimuli, name='Pair')
            p = p + 1
            h = pd.Series(vs, name='High')
            subj = pd.Series(np.full_like(p, subject), name='Subject')
            snum = pd.Series(np.full_like(p, sn), name='Session')
                
            dt = pd.Series(pd.Timestamp(date), name='Date')
            dt = dt.repeat(len(p))
            dt = dt.reset_index(drop=True)
            if sessionType == 'A':
                #st = pd.Series(np.full_like(p, 'point', np.str_), name='SessionType')
                st = pd.Series('point', name='SessionType')
                st = st.repeat(len(p))
                st = st.reset_index(drop=True)

                df = pd.concat([subj, st, snum,p, h, dt], axis=1)
            else:
                st = pd.Series('interval', name='SessionType')
                st = st.repeat(len(p))
                st = st.reset_index(drop=True)

                l, h = zip(*vs)
                l = pd.Series(l, name='Low')
                h = pd.Series(h, name='High')
                df = pd.concat([subj, st, snum, p, l, h, dt], axis=1)
            df.index.name = 'Order'
            df = df.sort_values('Pair')
            self.df = pd.concat((self.df, df))
            l, pos = self.search('system.stimuliset', pos, fEnd)
            if l is None:
                break
            stimuli = l[2]
            sEnd = fEnd


logs = scanDir(LOGDIR, PREFIX)
f = logs[1]
df = None
done = {}
for f in logs:
    print('\nProcessing log file: %s' % f)
    fs = f.split('_')
    subin = int(fs[1][4:])
    #if subin in done:
    #    print('    Warning: Skipping %s as higher version has be process already.' % f)
    #    continue

    try:
        ilp = IntervalLogParser(os.path.join(LOGDIR, f), dataFile=DATAFILE)
        ilp.parse()
        ilp.df
        if df is None:
            df = ilp.df
        else:
            df = pd.concat([ilp.df, df])

        done[subin] = True
    except IncompleteLogError as e:
        print('Error: %s' % e)
        continue
    except Exception as e:
        #raise
        print('Error: %s' % e)
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_tb(exc_traceback)
        #traceback.print_exc()
        
        continue

fn = time.strftime('int01_data_%y%m%d_%H%M.xlsx')

df.to_excel( os.path.join('.', fn), sheet_name='Responce')
